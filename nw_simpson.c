#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define f(x)(sqrt(1-(x*x*x)))

int main()
{
   float a,b,h,R,s,p,x;
   int n;
   void Simpson_One_Third_Rule( float , float, int , float);
   system("clear");
   printf("\n\t Enter the value of upper bound(b):");
   scanf("%f",&b);
   printf("\n\t Enter the value of lower bound(a):");
   scanf("%f",&a);
   printf("\n\t Enter the value of interval(n):");
   scanf("%d",&n);

   Simpson_One_Third_Rule(a,b,n,R);
   return(0); 
}


//------------------------SIMPSON_ONE_THIRD_RULE
void Simpson_One_Third_Rule(float a,float b, int n, float R)
{
   float h,s,p,x;
   h = (b-a)/n;
   s=0;
   x = a+h;
   while(x < b)
   {
      s = s+f(x);
      x = x+(2*h);
   }
   s = (4*s);
   p = 0;
   x = a+(2*h);
   while(x < b)
   {
      p = p+f(x);
      x = x+(2*h);
   }
   p = (2*p);
   R = f(a)+s+p+f(b);
   R = (h/3)*R;
   printf("\n\t R will store the result:%f\n\n", R);
   return;
}
