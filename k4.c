#include<stdio.h>
#define f(x,y) ((x*x) + (y*y))

int main(void)
{

  float x0, xr, h, k1, k2, k3, k4, y0, y1, R, k, x1;
  
  printf("\n Enter the initial value of y : ");
  scanf("%f", &y0);

  printf("\n Enter the value of xr : ");
  scanf("%f", &xr);

  printf("\n Enter the value of step length : ");
  scanf("%f", &h);

  printf("\n Enter the initial value of x : ");
  scanf("%f", &x0);

 
  while(x0 < xr)
   {
     k1 = h * f(x0,y0);
     printf("k1=%f",k1);
     k2 = h * f((x0+(h/2)),(y0+(k1/2)));
     printf("k2=%f",k2); 
    k3 = h * f((x0+(h/2)),(y0+(k2/2)));
     printf("k3=%f",k3);
     k4 = h * f((x0+h),(y0+k3));
     printf("k4=%f",k4);
     k = ((k1 + 2*k2 + 2*k3 + k4) / 6);
     
     y1 = y0 + k;
     printf("\n y(%f) = %f\n", (x0+h), y1);
     x0 = x0 + h;
     y0 = y1;
   }

  R = y0;
  printf("\n The value of  y(%f) = %f\n", xr, R);

  return(1);
}

     
