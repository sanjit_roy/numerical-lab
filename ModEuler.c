#include<stdio.h>
#define f(x,y) ((2-(y0*y0))/(5*x))

int main(void)
{
  float x1, x0, h, xr, y1, y0, E, e, y, R;
  
  printf("\n Enter the initial value of y = ");
  scanf("%f", &y0);

  printf("\n Enter the value of x0 : ");
  scanf("%f", &x0);


  printf("\n Enter the value of step size (h): ");
  scanf("%f", &h);

  printf("\n Enter the value of error tolerance (e) : ");
  scanf("%f",&e);

  printf("\n Enter the value of xr : ");
  scanf("%f",&xr);


  x1 = x0 + h;
  while(x1 <= xr)
   {
     y1 = y0 + (h * f(x0,y0));
   
     E = y1 - y0;
     while(E > e)
      {
        y = y0 + (h / 2) * (f(x0,y0) + f(x1,y1));
        printf("\ny = %f\n", y);

        E = y - y1;

        if(E < 0)
         {
           E = E * (-1);
         }

        y1 = y;
      }

    y0 = y1;
    x0 = x0 + h;
    x1 = x0 + h;
  }


  R = y0;

  printf("\n y(%f) = %f\n\n", xr, R);
  return(1);
}

         
