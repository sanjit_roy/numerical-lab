#include<stdio.h>
#include<stdlib.h>
int main(void)
{
  system("clear");
  int i, N, Flag, j;
  float XOLD[10], X[10], k, E, e, s, A[10][10], B[10], T;


  printf("\n\t Enter the number of rows and coloumns : ");
  scanf("%d", &N);

  printf("\n\t~~~~~~~~~~~~Enter the value of matrix A~~~~~~~~\n");
  i = 0;
  while(i < N)
   {
     j = 0;
     while(j < N)
      {
        printf("\n\t Enter the value of A[%d][%d] =  ",i,j);
        scanf("%f", &A[i][j]);
        j = j + 1;
      }
      i = i + 1;
   }

  printf("\n\n\n\t~~~~~~~~~~~~The A Matrix is as follows~~~~~~~~~~~ \n\n");
  i = 0;
  while(i < N)
   {
     j = 0;
     while(j < N)
       {
         printf("\t%f", A[i][j]);
         j = j + 1;
       }
       printf("\n");
       i = i + 1;
   }

   
  printf("\n\n\t~~~~~~Enter the value of Matrix B~~~~~~~~\n");
  i = 0;
  while(i < N)
   {
     printf("\n\t Enter the value of B[%d] = ", i);
     scanf("%f", &B[i]);
     i = i + 1;
   }


  printf("\n\n\t ~~~~~~~~~The Matrix B is as follows~~~~~~~\n");
  i = 0;
  while(i < N)
   {
     printf("%f\n",B[i]);
     i = i + 1;
   }


  printf("\n\t Enter the value of error tolerance : ");
  scanf("%f", &e);

//---------------------------------------------------------------------------  
  i = 0;
  while(i < N)
   {
     XOLD[i] = 0;
     i = i + 1;
   }


  Flag = 1;
  while(Flag == 1)
   {
     i = 0;
     Flag = 0;
     while(i < N)
      {
        j = 0;
        s = 0;
        while(j < N)
         {
           s = s + (A[i][j] * XOLD[j]);
           j = j + 1;
         }
         s = s - (A[i][i] * XOLD[i]);
         T = B[i] - s;
         X[i] = (T / A[i][i]);
         E = XOLD[i] - X[i];
         
         if(E < 0)
          {
            E = E * (-1);
          }

         if(E > e)
          {
            Flag = 1;
          }

          i = i + 1;
       }

     j = 0;
     while(j < N)
      {
        XOLD[j] = X[j];
        j = j + 1;
      }
   }



   printf("\n\n\t~~~~~~~~~~~~~The Value of unknowns are as follows~~~~~~~~\n\n");
  i = 0;
  while( i < N)
  {
    
    printf("%10.5f\n", XOLD[i]);
    i = i + 1;
  }  

 return(1);
}

  
