#include<stdio.h>
#include<stdlib.h>
#define f(x0,y0)(x0-y0)


int main()
{
   float x0,x,y0,y,h,R,k1,k2,k3,k4,k;
   void Runge_Kutta_Fourth_Order(float, float, float, float);
   system("clear");
   printf("\n\t Enter the value of lower bound(x0):");
   scanf("%f",&x0);
   printf("\n\t Enter the value of upper bound(y):");
   scanf("%f",&y);
   printf("\n\t Enter the initial(y0) value:");
   scanf("%f",&y0);
  
   Runge_Kutta_Fourth_Order(x0,y0,y,R);
   
   return(0);
}


//--------------------RUNGE_KUTTA_FOURTH_ORDER
void Runge_Kutta_Fourth_Order(float x0, float y0, float y, float R)
{ 
   float k,k1,k2,h,x;
   printf("\n\t Enter the value of step length(h):");
   scanf("%f",&h);
   x = x0+h;
   while(x0 < y)
   {
      k1 = h*f(x0,y0);
      k2 = h*f((x0+h),(y0+k1));
      k  = ((k1+k2)/2);
      y0 = y0+k;
      x0 = x;
      x = x0+h;
   }
   R = y0;
   printf("\n\t R will store the result:%f\n",R);
   return;
}
