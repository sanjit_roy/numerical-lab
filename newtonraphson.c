#include<stdio.h>
#include<stdlib.h>
#define f(x) ((x*x) + (2*x) - 2)
#define fd(x) ((2*x) + 2)


int main(void)
{

 float x1, x2, e, h, xr, p;

 printf("\n\t Enter the value of error tolerance : ");
 scanf("%f", &e);
 
 x1 = 0.0;
 x2 = x1 + 1;
 
 while( f(x1) * f(x2) > 0 )
 {
     x1 = x1 + 1;
     x2 = x2 + 1;
 }


 p = f(x1);

 if( p < 0 )
  {
    p =  p * (-1);
  }

 
 while( p > e )
  {
     h = ( f(x1) / fd(x1) );
     h =  h * (-1) ;
     x1 = x1 + h;
     p = f(x1);
    
     
     if( p < 0 )
     {
       p =  p * (-1);
     }

  }

  xr = x1;

  printf("\n\t The root of the given equation is = %f\n", xr);
  
  return(1);
}



 
