#include<stdio.h>
#define f(x) ((x*x*x)- (4*x) - 9)


void main(void)
{
  


  float x1, x2, P, m, e, x;
  m = 0.0;
  P = 0.0;
   
  printf("\n\t Enter the value of error tolerance : " );
  scanf("%f", &e);

  x1 = 0.0;
  x2 = x1 + 1.0;

  while( (( f(x1) * f(x1+1) ) > 0 ))
   {
     x1 = x1 + 1;

   }
    x1=x1-1;
     x2 = x1 + 1;
//printf("x1=%f\n",x1);
//printf("x2=%f\n",x2);

   if( x1 == x2 )
   {
     printf("\n No Root Found \n");
     return;
   }
  
   
   
   if( f(x1) == 0 )
   {
     x = x1;
     return;
   }



   if( f(x2) == 0 )
   {
     x = x2;
     return;
   }


 
   P = x1 - x2;

   if( P < 0 )
    {
      P = ( P * (-1) );
    }


   while( P > e )
   {
     m = ( ( x1 + x2 ) / 2 );
//printf("\t\tF[m]=%f\n",f(m));

     if( f(m) == 0 ) 
        {
           x = m;
           return;
        }
     
     else if( f(x1) * f(m) < 0 )
        {
           x2 = m;
        }
     else
        {
           x1 = m;
        }
      
     P = x1 - x2;

     if( P < 0 )
       {
           P = ( P * (-1) );
       }
    

   }


  x = m;
  printf("\n\t The root of the given equation is %f\n", x);

  return;
 

}
  
         

